from slacktory import requests, history_url, data, headers


def test_get_slack_message():
    msg = requests.post(history_url, data=data, headers=headers).json()['messages'][0]
    assert (msg['type'] == 'message')

